﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using InterceptorTest.Models;
using InterceptorTest.Common;
using Microsoft.AspNetCore.Authorization;

namespace InterceptorTest.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// 局部的使用拦截器
        /// </summary>
        /// <returns></returns>
        [Action]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 不需要登录使用这个[NoSign] 拦截器
        /// </summary>
        /// <returns></returns>
        [NoSign]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
