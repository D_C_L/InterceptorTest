﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InterceptorTest.Common
{
    /// <summary>
    /// IExceptionFilter 异常拦截
    /// ActionFilterAttribute 请求拦截器
    /// </summary>
    public class ActionAttribute: ActionFilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// 在控制器执行之前调用
        /// </summary>
        /// <param name="context">执行的上下文</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            // 判断是否加上了不需要拦截
            var noNeedCheck = false;
            if (context.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
            {
                noNeedCheck = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                  .Any(a => a.GetType().Equals(typeof(NoSignAttribute)));
            }
            if (noNeedCheck) return;

            context.HttpContext.Response.WriteAsync("在控制器执行之前调用");
        }

        /// <summary>
        /// 在控制器执行之后调用
        /// </summary>
        /// <param name="context">执行的上下文</param>
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            // 判断是否加上了不需要拦截
            var noNeedCheck = false;
            if (context.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
            {
                noNeedCheck = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                  .Any(a => a.GetType().Equals(typeof(NoSignAttribute)));
            }
            if (noNeedCheck) return;

            context.HttpContext.Response.WriteAsync("在控制器执行之后调用");
        }

        /// <summary>
        /// 在返回数据执行之前调用
        /// </summary>
        /// <param name="context">执行的上下文</param>
        public override void OnResultExecuting(ResultExecutingContext context)
        {
            // 判断是否加上了不需要拦截
            var noNeedCheck = false;
            if (context.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
            {
                noNeedCheck = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                  .Any(a => a.GetType().Equals(typeof(NoSignAttribute)));
            }
            if (noNeedCheck) return;

            context.HttpContext.Response.WriteAsync("在返回数据执行之前调用");
        }

        /// <summary>
        /// 在返回数据执行之后调用
        /// </summary>
        /// <param name="context">执行的上下文</param>
        public override void OnResultExecuted(ResultExecutedContext context)
        {
            // 判断是否加上了不需要拦截
            var noNeedCheck = false;
            if (context.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
            {
                noNeedCheck = controllerActionDescriptor.MethodInfo.GetCustomAttributes(inherit: true)
                  .Any(a => a.GetType().Equals(typeof(NoSignAttribute)));
            }
            if (noNeedCheck) return;

            context.HttpContext.Response.WriteAsync("在返回数据执行之后调用");
        }

        /// <summary>
        /// 当然是发生异常时被调用了
        /// </summary>
        /// <param name="context">执行的上下文</param>
        public void OnException(ExceptionContext context)
        {
            context.HttpContext.Response.WriteAsync("当然是发生异常时被调用了");
            context.ExceptionHandled = true;//异常已经处理，不要再次处理了
        }
    }

    /// <summary>
    /// 不需要登陆的地方加个这个空的拦截器
    /// </summary>
    public class NoSignAttribute : ActionFilterAttribute { }
}
